defmodule ContentfulexWeb.RecipeControllerTest do
  use ContentfulexWeb.ConnCase

  describe "index" do
    test "lists all recipes", %{conn: conn} do
      conn = get(conn, Routes.recipe_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Recipes"
    end
  end

  describe "show recipe" do
    test "redirects to show when data is valid", %{conn: conn} do
      id = "437eO3ORCME46i02SeCW46"

      conn = get(conn, Routes.recipe_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show Recipe"
    end
  end
end
