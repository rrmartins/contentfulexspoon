defmodule Contentfulex.RecipesTest do
  use Contentfulex.DataCase

  alias Contentfulex.Recipes

  describe "list_recipes/0" do
    test "returns all recipes" do
      assert list_recipes = Recipes.list_recipes()

      assert Enum.count(list_recipes) == 4
      assert %{title: title_one} = Enum.at(list_recipes, 0)
      assert %{title: title_two} = Enum.at(list_recipes, 1)
      assert %{title: title_three} = Enum.at(list_recipes, 2)
      assert %{title: title_four} = Enum.at(list_recipes, 3)

      assert title_one == "White Cheddar Grilled Cheese with Cherry Preserves & Basil"
      assert title_two == "Tofu Saag Paneer with Buttery Toasted Pita"
      assert title_three == "Grilled Steak & Vegetables with Cilantro-Jalapeño Dressing"
      assert title_four == "Crispy Chicken and Rice\twith Peas & Arugula Salad"
    end
  end

  describe "get_recipe!/1" do
    test "returns not found with an id invalid" do
      assert Recipes.get_recipe!("3TJp6aDAcMw6yMiE82Oy0K") == "Not found"
    end

    test "returns recipe with an id valid" do
      assert Recipes.get_recipe!("437eO3ORCME46i02SeCW46") == %{
               description:
                 "Crispy chicken skin, tender meat, and rich, tomatoey sauce form a winning trifecta of delicious in this one-pot braise. We spoon it over rice and peas to soak up every last drop of goodness, and serve a tangy arugula salad alongside for a vibrant boost of flavor and color. Dinner is served! Cook, relax, and enjoy!",
               id: "437eO3ORCME46i02SeCW46",
               title: "Crispy Chicken and Rice\twith Peas & Arugula Salad"
             }
    end
  end
end
