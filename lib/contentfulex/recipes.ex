defmodule Contentfulex.Recipes do
  @moduledoc """
  The Contentfulexs context.
  """

  import Contentful.Query
  alias Contentful.Delivery.Entries

  def list_recipes() do
    Entries
    |> content_type("recipe")
    |> fetch_all
    |> handle_entries()
  end

  def get_recipe!(id) do
    Entries |> fetch_one(id) |> handle_entry()
  end

  defp handle_entries({:ok, entries, total: _}) do
    Enum.map(entries, &parse/1)
  end

  defp handle_entries({:error, :not_found, _}), do: "Not found."

  defp handle_entry({:ok, entry}) do
    parse(entry)
  end

  defp handle_entry({:error, :not_found, _}), do: "Not found"

  defp parse(entry) do
    case entry do
      %{
        sys: %{id: id},
        fields: %{"description" => description, "title" => title}
      } ->
        %{id: id, description: description, title: title}

      _ ->
        %{}
    end
  end
end
