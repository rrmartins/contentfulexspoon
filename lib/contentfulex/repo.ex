defmodule Contentfulex.Repo do
  use Ecto.Repo,
    otp_app: :contentfulex,
    adapter: Ecto.Adapters.Postgres
end
