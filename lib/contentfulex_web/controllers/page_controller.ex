defmodule ContentfulexWeb.PageController do
  use ContentfulexWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
