# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :contentfulex,
  ecto_repos: [Contentfulex.Repo],
  generators: [binary_id: true]

# Configures the endpoint
config :contentfulex, ContentfulexWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "WnE5cCO0QS45UvSdtyxdFZLjcb5hrwe8pe5GrSpeqDNkC1k0vEEN74jm7TzSc9JA",
  render_errors: [view: ContentfulexWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Contentfulex.PubSub,
  live_view: [signing_salt: "pPQR93vP"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :contentful,
  delivery: [
    space_id: "kk2bw5ojx476",
    access_token: "7ac531648a1b5e1dab6c18b0979f822a5aad0fe5f1109829b8a197eb2be4b84c",
    environment: "master"
  ]

config :contentfulex,
  management: [
    coming: :soon
  ]

config :contentfulex,
  preview: [
    coming: :soon
  ]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
